package br.com.jdo.ibm.customer;

import java.util.Date;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import br.com.jdo.ibm.customer.model.COrder;
import br.com.jdo.ibm.customer.model.Customer;
import br.com.jdo.ibm.customer.service.COrderService;
import br.com.jdo.ibm.customer.util.DateUtil;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@SpringBootTest
public class CustomerServicesTests {

	@Autowired
	private COrderService cOrderService;
	
	@Test
	public void testOrdersOrderedByTotalAmountAsc() throws Exception {
		List<COrder> pCorder = cOrderService.findOrderByTotalAmount();
		assert(pCorder.size() > 10);
	}
	
	@Test
	public void testGrestestOrder2016() throws Exception {
		String year = "2016";
		Date dateStart = DateUtil.parseYYYY(year);
		Date dateEnd = DateUtil.endOfYear(dateStart);
		log.info("dateStart:" + dateStart + " - dateEnd:" + dateEnd);
		COrder cOrder = cOrderService.findFirstByDateBetweenOrderByTotalAmountDesc(dateStart, dateEnd);
		assert(cOrder != null);
	}

	@Test
	public void testGrestestCustomers() throws Exception {
		List<Customer> customerList = cOrderService.findByGreatherTotalAmount();
		assert(customerList.size() > 2);
	}
	
	@Test
	public void testGrestestMoreSelled() throws Exception {
		String category = cOrderService.findRecommendationByCategory();
		assert(category != null);
	}


}
