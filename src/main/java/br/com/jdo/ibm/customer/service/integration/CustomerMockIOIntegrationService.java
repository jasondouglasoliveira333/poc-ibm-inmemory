package br.com.jdo.ibm.customer.service.integration;

import java.util.List;

import br.com.jdo.ibm.customer.model.COrder;
import br.com.jdo.ibm.customer.model.Customer;
import retrofit2.Call;
import retrofit2.http.GET;

public interface CustomerMockIOIntegrationService {

	@GET("598b16861100004905515ec7")
	public Call<List<COrder>> orders();
	
	@GET("598b16291100004705515ec5")
	public Call<List<Customer>> customers();
	
}

