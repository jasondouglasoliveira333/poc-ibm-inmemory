package br.com.jdo.ibm.customer.config;

import java.util.concurrent.TimeUnit;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import br.com.jdo.ibm.customer.service.integration.CustomerMockIOIntegrationService;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

@Configuration
public class CustomerConfig {

	@Value("${br.com.jdo2.ibm.customer.mock.baseurl}")
	private String mockIOBaseurl = "http://www.mocky.io/v2/";

	@Bean
	public CustomerMockIOIntegrationService createService() {
		OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
		httpClient.readTimeout(1, TimeUnit.MINUTES);
		httpClient.writeTimeout(1, TimeUnit.MINUTES);

		Gson gson = new GsonBuilder().setDateFormat("dd-MM-yyyy").create();

		Retrofit retrofit = new Retrofit.Builder().baseUrl(mockIOBaseurl)
				.addConverterFactory(GsonConverterFactory.create(gson)).client(httpClient.build()).build();

		return retrofit.create(CustomerMockIOIntegrationService.class);
	}

}
