package br.com.jdo.ibm.customer.controller;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.jdo.ibm.customer.dto.RecomentationDTO;
import br.com.jdo.ibm.customer.model.COrder;
import br.com.jdo.ibm.customer.model.Customer;
import br.com.jdo.ibm.customer.service.COrderService;
import br.com.jdo.ibm.customer.util.DateUtil;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@CrossOrigin
@RequestMapping("customers")
@RestController
public class CustomerController {
	
	@Autowired
	private COrderService cOrderService;
	
	@GetMapping("orders")
	public ResponseEntity<?> findOrderByTotalAmount(){
		try {
			List<COrder> pCorder = cOrderService.findOrderByTotalAmount();
			log.info("pCorder:" + pCorder);
			return ResponseEntity.ok(pCorder);
		}catch(Exception e) {
			e.printStackTrace();
			return ResponseEntity.badRequest().body(null);
		}
	}

	@GetMapping("orders/{year}/highest")
	public ResponseEntity<?> highestSell(@PathVariable String year){
		try {
			Date dateStart = DateUtil.parseYYYY(year);
			Date dateEnd = DateUtil.endOfYear(dateStart);
			log.info("dateStart:" + dateStart + " - dateEnd:" + dateEnd);
			COrder cOrder = cOrderService.findFirstByDateBetweenOrderByTotalAmountDesc(dateStart, dateEnd);
			log.info("cOrder:" + cOrder);
			return ResponseEntity.ok(cOrder);
		}catch(Exception e) {
			e.printStackTrace();
			return ResponseEntity.badRequest().body(null);
		}
	}

	@GetMapping("fidelity")
	public ResponseEntity<?> fidelity(){
		try {
			List<Customer> customerList = cOrderService.findByGreatherTotalAmount();
			log.info("customerList:" + customerList);
			return ResponseEntity.ok(customerList);
		}catch(Exception e) {
			e.printStackTrace();
			return ResponseEntity.badRequest().body(null);
		}
	}

	@GetMapping("recommentations")
	public ResponseEntity<?> recommentations(){
		try {
			RecomentationDTO r = new RecomentationDTO();
			String category = cOrderService.findRecommendationByCategory();
			r.setCategory(category);
			return ResponseEntity.ok(r);
		}catch(Exception e) {
			e.printStackTrace();
			return ResponseEntity.badRequest().body(null);
		}
	}


}
