package br.com.jdo.ibm.customer.model;

import java.math.BigDecimal;

import com.google.gson.annotations.SerializedName;

import lombok.Data;

@Data
public class Item {

	@SerializedName("codigo")
	private String code;
	@SerializedName("produto")
	private String product;
	@SerializedName("variedade")
	private String variety;
	@SerializedName("pais")
	private String country;
	@SerializedName("categoria")
	private String category;
	@SerializedName("safra")
	private String vintage;
	@SerializedName("preco")
	private BigDecimal price;

	@Override
	public String toString() {
		return "Item [code=" + code + ", product=" + product + ", variety=" + variety + ", country="
				+ country + ", category=" + category + ", vintage=" + vintage + ", price=" + price + "]";
	}
	
	
		
}
