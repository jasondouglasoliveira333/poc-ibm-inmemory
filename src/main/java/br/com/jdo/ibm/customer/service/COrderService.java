package br.com.jdo.ibm.customer.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.jdo.ibm.customer.model.COrder;
import br.com.jdo.ibm.customer.model.Customer;
import br.com.jdo.ibm.customer.service.integration.CustomerMockIOIntegrationService;
import br.com.jdo.ibm.customer.util.StringUtil;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class COrderService {
	
	private static final Random RANDOM = new Random();

	@Autowired
	private CustomerMockIOIntegrationService customerMockIOIntegrationService;
	
	public List<COrder> findOrderByTotalAmount() throws Exception {
		List<COrder> orders = customerMockIOIntegrationService.orders().execute().body();
		List<COrder> ordersOrdered = orders.stream().sorted((a,b) -> a.getTotalAmount().compareTo(b.getTotalAmount())).toList();
		return ordersOrdered;
	}

	public COrder findFirstByDateBetweenOrderByTotalAmountDesc(Date dateStart, Date dateEnd) throws Exception {
		List<COrder> orders = customerMockIOIntegrationService.orders().execute().body();
		List<COrder> ordersFilteredOrdered = orders.stream()
				.filter(a -> a.getDate().getTime() > dateStart.getTime() && a.getDate().getTime() < dateEnd.getTime())
				.sorted((a,b) -> a.getTotalAmount().compareTo(b.getTotalAmount())).toList();
		return ordersFilteredOrdered.get(ordersFilteredOrdered.size()-1);
	}
	
	//Thera is a better way to do this
	public List<Customer> findByGreatherTotalAmount() throws Exception {
		List<COrder> orders = customerMockIOIntegrationService.orders().execute().body();
		Map<String, Double> customerSells = orders.stream().collect(Collectors.groupingBy(COrder::getCustomerCpf, 
				Collectors.summingDouble(c -> c.getTotalAmount().doubleValue())));
		List<Entry<String, Double>> customerEntries = customerSells.entrySet().stream()
				.sorted((a, b) -> b.getValue().compareTo(a.getValue())).toList();
		log.info("customerEntries:" + customerEntries);
		List<Customer> customers = customerMockIOIntegrationService.customers().execute().body();
		Map<String, Customer> customersMap = customers.stream().collect(Collectors.toMap(Customer::getCpf, Function.identity()));
		List<Customer> customersResult = new ArrayList<>();
		customerEntries.subList(0, 3).forEach(cpf -> 
			customersResult.add(customersMap.get(StringUtil.adjustCpf(cpf.getKey())))
		);
		return customersResult;
	}
	
	public String findRecommendationByCategory() throws Exception {
		List<COrder> orders = customerMockIOIntegrationService.orders().execute().body();
		Map<String, Integer> categorySells = new HashMap<>();
		orders.stream().forEach(o -> {
			o.getItens().forEach(item -> {
				Integer total = categorySells.get(item.getCategory());
				if (total == null) {
					total = 0; 
				}
				total++;
				categorySells.put(item.getCategory(), total);
			});
		});
		List<Entry<String, Integer>> categoryEntries = categorySells.entrySet().stream()
				.sorted((a, b) -> b.getValue().compareTo(a.getValue())).toList();
		log.info("categoryEntries:" + categoryEntries);
		int MAX = Math.min(categoryEntries.size(), 10);
		categoryEntries = categoryEntries.subList(0, MAX);
		int itemSelected = RANDOM.nextInt(MAX);
		return categoryEntries.get(itemSelected).getKey();

	}


}
