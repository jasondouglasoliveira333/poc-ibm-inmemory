package br.com.jdo.ibm.customer.model;

import com.google.gson.annotations.SerializedName;

import lombok.Data;

@Data
public class Customer {
	
	private Long id;

	@SerializedName("nome")
	private String name;
	
	private String cpf;

}
