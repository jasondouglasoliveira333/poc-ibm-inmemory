package br.com.jdo.ibm.customer.model;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.google.gson.annotations.SerializedName;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class COrder {

	@SerializedName("codigo")
	private String code;
	@SerializedName("data")
	private Date date;
	@SerializedName("valorTotal")
	private BigDecimal totalAmount;
	@SerializedName("cliente")
	private String customerCpf;
	
	private List<Item> itens;
	
}
